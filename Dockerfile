FROM tetradecahedron/baseimage-alpine
LABEL maintainer="tetradecahedron"

# Copy local files to container
COPY files/ /

# Install and configure base utilities 
RUN \
	echo "**** install build packages ****" && \
	apk add --no-cache \
		nginx \
		logrotate \
		sqlite \
		curl \
		unzip \
		php7 \
		php7-fpm \
		php7-cli \
		php7-curl \
		php7-ctype \
		php7-dom \
		php7-fileinfo \
		php7-json \
		php7-intl \
		php7-iconv \
		php7-gmp \
		php7-session \
		php7-simplexml \
		php7-zlib \
		php7-gd \
		php7-mbstring \
		php7-sqlite3 \
		php7-pdo_sqlite \
		php7-tidy \
		php7-xmlwriter \
		php7-xml && \
	echo "**** fix logrotate ****" && \
	sed -i "s#/var/log/messages {}.*# #g" /etc/logrotate.conf

# Expose ports on container
EXPOSE 80

# Set workdir & Expose a volume
WORKDIR /config
VOLUME /config
