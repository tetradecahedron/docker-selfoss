Set userid and groupid of container with environment vars
PUID=<uid(userid)>
PGID=<gid(groupid)>

Mount the volume /config for persistence
